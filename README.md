# pygfpp
A  python package to read Ensoft Group output summary file and make the data available for post processing, either in nested dicts or as `pandas` DataFrame. This is a work in progress. Once the data is available then it is up to the user to manipulate the data to extract relevant information.

## Introduction
This is a first attempt to read in summary file from [Enfosft Group](https://www.ensoftinc.com/products/group/). The package reads all the lines from the output summary file up to the `*****     SUMMARY FOR LOAD CASES AND COMBINATIONS      *****` line. The number of piles in the group is then determined from the number of piles in the pfactor table. Then the indices of either `LOAD CASE:` or `LOAD COMB.:` are stored. The first line of various tables of interests have been hard coded from the position of the `LOAD CASE:` or `LOAD COMB. :` line. The read tables are then returned as a nested python dict.


## How to prepare Ensoft Group output file
There are a number of options that can be choosen to output various variables to the summary file. This changes the number of columns in the lateral pile analysis table. Here the summary file means the output file `summary file extension here`. The lateral load analysis table has a specific number of columns corresponding to the variables choosen. It is difficult to account for every combination of the output variables.  This package was developed using a output file which outputs all the variables, i.e.  Add bit more on what options to choose for the output variables.

The file data is read from the start of the file to `*****     SUMMARY FOR LOAD CASES AND COMBINATIONS      *****` line. The below this line is discarded and hopefully can be reproduces by manipulating the data in the file above this line.

## Examples
There is one example for reading the load combination and one example for reading load cases in the `examples` folder. These are not yet complete. If anybody finds this package and is curious they may look at those examples for now to see how the package works.