from pathlib import Path
from pygfpp import LoadCase
import pygfpp.utility_functions as utf

# read the summary file
# and get the data for load cases
# data includes
# 1) the pfactors, 
# 2) local and global axes pile top displacements
# 3) minimum and maximum lateral analysis values for piles
# for lateral pile analysis select all variables

def main():
    path_root = Path(r"./")
    path_data = path_root / "data"
    fname_in = "01_load_cases_input.txt"
    fname_out = "01_load_cases_output.json"

    fin = path_data.joinpath(fname_in)
    fout = path_data.joinpath(fname_out)

    # read in summary file and data from load cases
    ldcase = LoadCase(fin)
    
    # number of piles in the group
    print(f"Number of piles: {ldcase.number_of_piles}")

    # write the data to a json file
    ldcase.to_json(fout)

if __name__ == "__main__":
    main()
