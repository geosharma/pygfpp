from pathlib import Path
from pygfpp import LoadComb
import pygfpp.utility_functions as utf
import pandas as pd

# Read load combination data


def main():
    path_root = Path(r"./")
    path_data = path_root / "data"
    fname_in = "03_Tucker Tank DIPP.gp12t"

    fin = path_data.joinpath(fname_in)

    ldcomb = LoadComb(fin)
    # print(ldcomb.case_indices)
    # print(ldcomb.pfactors)
    # df_pf = utf.dict2df(ldcomb.pfactors)
    # print(ldcomb.number_of_piles)
    # print(ldcomb.pile_displacements)
    df_disp = utf.dict2df(ldcomb.pile_displacements)
    # print(df_disp)
    df = df_disp.groupby(by=["case", "axis"]).aggregate({"disp-y_in":["min", "max"], "disp-z_in": ["min", "max"]})
    df = df.reset_index()
    df.columns = df.columns.to_flat_index()
    print(df)
    cols = df.columns.to_list()
    ncols = []
    for col in cols:
        ncols.append("_".join(c for c in col if c != ""))
    df.columns = ncols
    print(df)
    # print(ldcomb.pile_reactions)
    # df_reaction = utf.dict2df(ldcomb.pile_reactions)
    # idxs = df_reaction.groupby(by=["case", "axis"])["axial_k"].idxmax()
    # print(df_reaction.loc[idxs])
    # print(ldcomb.pile_lateral_analysis)
    df_lat = utf.dict2df(ldcomb.pile_lateral_analysis)
    # print(df_lat.columns)
    df_lat = df_lat[
        [
            "case",
            "min_max",
            "pile_no",
            "disp-x_in",
            "disp-y_in",
            "disp-z_in",
            "disp-yz_in",
            "mom-z_k-in",
            "mom-y_k-in",
            "mom-yz_k-in",
            "shear-y_k",
            "shear-z_k",
            "shear-yz_k",
            "axial_k",
        ]
    ]

    df_lat["max-mom_k-in"] = df_lat[["mom-z_k-in", "mom-y_k-in", "mom-yz_k-in"]].apply(abs).max(axis=1)
    df_lat["max-disp_in"] = df_lat[["disp-x_in", "disp-z_in", "disp-yz_in"]].apply(abs).max(axis=1)
    df_lat["max-shear_k"] = df_lat[["shear-y_k", "shear-z_k", "shear-yz_k"]].apply(abs).max(axis=1)
    df_max_axial = df_lat.loc[df_lat.groupby(["case"])["axial_k"].idxmax()]
    df_max_moment = df_lat.loc[df_lat.groupby(["case"])["max-mom_k-in"].idxmax()]
    # print(df_max_axial)
    # print(df_max_moment)

if __name__ == "__main__":
    main()
