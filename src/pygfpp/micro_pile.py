import math


class Micropile(object):
    def __init__(self, diameter=5.5, thickness=0.515, corrosion=0.0625) -> None:
        self._do = diameter
        self._t = thickness
        self._tc = corrosion
        # inside diameter
        self._di = self._do - 2 * self._t
        # corrosion corrected outside diameter
        self._dc = self._do - 2 * self._tc
        # corrosion corrected cross section area
        self._area = math.pi / 4 * (self._dc**2 - self._di**2)
        # outside diameter for joint at half the tickness
        self._dj = self._do - self._t
        # cross section area at the joint
        self._areaj = math.pi / 4 * (self._dj**2 - self._di**2)
        # section modulus
        self._sxx = math.pi / 32 * (self._dc**4 - self._di**4) / self._dc
        
        pass
