import os
import json
from itertools import takewhile


class base_ld_case_comb(object):
    def __init__(self, fpath: str | os.PathLike, search_string: str = "") -> None:
        """A abstract class for extracting relevant data for various load cases
        and combinations from GROUP output summary file.
        fpath: path or str, complete path to the GROUP output summary file.
        search_string: search string for either load combination files "LOAD COMB. :" or
        "LOAD CASE :" for load case files.
        """
        self._fpath: str | os.PathLike = fpath
        self._summary_line = (
            "*****     SUMMARY FOR LOAD CASES AND COMBINATIONS      *****"
        )
        # read the input file and return lines of the data file
        # up till the summary line above in the file
        self._lines: list = self._read_data()
        self._search_string: str = search_string
        self._case_names: list[str] = []
        self._case_idxs: list[int] = []
        self._case_names, self._case_idxs = self._get_case_names_idxs()
        self._ncases: int = len(self._case_names)
        self._offsets: dict = self._get_offsets()
        self._case_length: int = self._get_case_length()
        self._npiles: int = self._get_npiles()
        self._pfactors: dict = self._get_pfactors()
        self._pile_displacements: dict = self._get_pile_displacements()
        self._pile_reactions: dict = self._get_pile_reactions()
        self._pile_lateral_analysis: dict = self._get_lateral_values()

    @property
    def case_names(self) -> list[str]:
        """Return a list of load case and load combination names."""
        return self._case_names

    @property
    def case_indices(self) -> list[int]:
        """Return the line indices of the of start of load case or load combination."""
        return self._case_idxs

    @property
    def ncases(self) -> int:
        """Return the number of cases in the output file."""
        return self._ncases

    @property
    def case_length(self) -> int:
        return self._case_length

    @property
    def pfactors(self, return_structure="pandas"):
        """Format the p-factors and return
        return_structure: "pandas" or "dict" whether to return the data
        as pandas dataframe or a dict"""

        return self._pfactors

    @property
    def number_of_piles(self) -> int:
        """Number of piles in the pile group"""
        return self._npiles

    @property
    def pile_displacements(self) -> dict:
        return self._pile_displacements

    @property
    def pile_reactions(self) -> dict:
        return self._pile_reactions

    @property
    def pile_lateral_analysis(self) -> dict:
        return self._pile_lateral_analysis

    @property
    def offsets(self) -> dict:
        return self._offsets

    def _nestedlist2dict(self, data_list) -> dict:
        """Format nested lists as dictionary."""
        data_dict = {}
        for line in data_list:
            line = line.strip().split()
            data_dict[int(line[0])] = [float(item) for item in line[1:]]
        return data_dict

    def _read_data(self) -> list:
        """Read the in the summary output file from Group"""
        with open(self._fpath, "r") as f:
            return list(
                takewhile(lambda line: self._summary_line not in line.strip(), f)
            )

    def _get_case_names_idxs(self) -> None:
        """Get the line indices of all lines that starts with the search_string."""
        case_names = []
        case_idxs = []
        for idx, line in enumerate(self._lines):
            if line.strip().startswith(self._search_string):
                ldcomb_name = self._lines[idx + 1].strip().split()
                case_names.append(" ".join(ldcomb_name[3:]))
                case_idxs.append(idx)
        return (case_names, case_idxs)

    def _get_npiles(self) -> int:
        """Number of piles in a group"""
        for idx, line in enumerate(
            self._lines[self._case_idxs[0] : self._case_idxs[1]]
        ):
            if line.strip() == "GROUP NO     P-FACTOR     Y-FACTOR":
                self._offsets["pfactors"] = idx + 2
                break
        npiles = 0
        for line in self._lines[self._case_idxs[0] + self._offsets["pfactors"] :]:
            if line == "\n":
                break
            else:
                npiles += 1
        return npiles

    def _get_offsets(self) -> dict:
        """Offsets, number of lines, to the first line in respective data table from the line with
        load cases and combinations names."""
        search_strings = (
            ["GROUP NO     P-FACTOR     Y-FACTOR"]
            + ["* PILE TOP DISPLACEMENTS *", "* PILE TOP REACTIONS *"] * 2
            + ["* MINIMUM VALUES AND LOCATIONS *", "* MAXIMUM VALUES AND LOCATIONS *"]
        )
        idxs = []
        for idx, line in enumerate(
            self._lines[self._case_idxs[0] : self._case_idxs[1]]
        ):
            if any(item in line for item in search_strings):
                idxs.append(idx)
        return {
            "offsets": {
                "pfactors": idxs[0] + 2,
                "pile_disp_global": idxs[1] + 4,
                "pile_disp_local": idxs[3] + 4,
                "pile_reaction_global": idxs[2] + 4,
                "pile_reaction_local": idxs[4] + 4,
                "lat_min_vals": idxs[5] + 6,
                "lat_max_vals": idxs[6] + 6,
            }
        }

    def _get_case_length(self) -> int:
        """Number of lines in one case block"""
        diff = [j - i for i, j in zip(self._case_idxs[:-1], self._case_idxs[1:])]

        if len(set(diff)) == 1:
            diff = diff[0]
        else:
            diff = 0
            raise ValueError(f"he data blocks are not of equal lengths: {diff}")
        return diff

    def _get_pfactors(self) -> list[float]:
        """p-factors for each pile in the group."""
        data_dict = {}
        data_dict["pfactors"] = {}
        for idx, case_idx in enumerate(self._case_idxs):
            start_idx = case_idx + self._offsets["pfactors"]
            end_idx = start_idx + self._npiles
            data = self._lines[start_idx:end_idx]
            data_dict["pfactors"][idx + 1] = self._nestedlist2dict(data)
        return data_dict

    def _get_pile_displacements(self):
        """Pile top displacements both global and local axes"""
        data_dict = {}
        data_dict["pile_disp"] = {}
        for idx, case_idx in enumerate(self._case_idxs):
            start_idx_global = case_idx + self._offsets["offsets"]["pile_disp_global"]
            end_idx_global = start_idx_global + self._npiles
            start_idx_local = case_idx + self._offsets["offsets"]["pile_disp_local"]
            end_idx_local = start_idx_local + self._npiles
            data_global = self._lines[start_idx_global:end_idx_global]
            data_local = self._lines[start_idx_local:end_idx_local]
            data_dict["pile_disp"][idx + 1] = {}
            data_dict["pile_disp"][idx + 1]["global_axes"] = self._nestedlist2dict(
                data_global
            )
            data_dict["pile_disp"][idx + 1]["local_axes"] = self._nestedlist2dict(
                data_local
            )
        return data_dict

    def _get_pile_reactions(self):
        """Pile top reactions both global and local axes"""
        data_dict = {}
        data_dict["pile_reaction"] = {}
        for idx, case_idx in enumerate(self._case_idxs):
            start_idx_global = (
                case_idx + self._offsets["offsets"]["pile_reaction_global"]
            )
            end_idx_global = start_idx_global + self._npiles
            start_idx_local = case_idx + self._offsets["offsets"]["pile_reaction_local"]
            end_idx_local = start_idx_local + self._npiles
            data_global = self._lines[start_idx_global:end_idx_global]
            data_local = self._lines[start_idx_local:end_idx_local]
            data_dict["pile_reaction"][idx + 1] = {}
            data_dict["pile_reaction"][idx + 1]["global_axes"] = self._nestedlist2dict(
                data_global
            )
            data_dict["pile_reaction"][idx + 1]["local_axes"] = self._nestedlist2dict(
                data_local
            )
        return data_dict

    def _get_lateral_values(self) -> dict:
        """Minimum and Maximum Tables of lateral pile analysis."""
        data_dict = {}
        data_dict["lateral_analysis"] = {}
        for idx, case_idx in enumerate(self._case_idxs):
            start_idx_min = case_idx + self._offsets["offsets"]["lat_min_vals"]
            end_idx_min = start_idx_min + 2 * self._npiles
            start_idx_max = case_idx + self._offsets["offsets"]["lat_max_vals"]
            end_idx_max = start_idx_max + 2 * self._npiles
            data_min = self._lines[start_idx_min:end_idx_min:2]
            data_max = self._lines[start_idx_max:end_idx_max:2]
            data_dict["lateral_analysis"][idx + 1] = {}
            data_dict["lateral_analysis"][idx + 1]["min_vals"] = self._nestedlist2dict(
                data_min
            )
            data_dict["lateral_analysis"][idx + 1]["max_vals"] = self._nestedlist2dict(
                data_max
            )
        return data_dict

    def to_json(self, fpath) -> None:
        """Write the file contents, offsets, pfactors, pile displacements, pile reactions,
        and lateral pile analysis results to json."""
        dict2save = {
            **self._offsets,
            **self._pfactors,
            **self._pile_displacements,
            **self._pile_reactions,
            **self._pile_lateral_analysis,
        }
        with open(fpath, "w") as f:
            json.dump(dict2save, f)


class LoadComb(base_ld_case_comb):
    def __init__(self, fpath: str | os.PathLike) -> None:
        """A class for extracting relevant data for various load combinations from GROUP output summary file.
        fpath: path or str, complete path to the GROUP output summary file
        colnames: a list of list of column names
        """
        self._fpath = fpath
        self._search_string: str = "LOAD COMB. :"
        base_ld_case_comb.__init__(self, fpath, self._search_string)


class LoadCase(base_ld_case_comb):
    def __init__(self, fpath: str | os.PathLike) -> None:
        """A class for extracting relevant data for various load combinations from GROUP output summary file.
        fpath: path or str, complete path to the GROUP output summary file
        colnames: a list of list of column names
        """
        self._fpath = fpath
        self._search_string: str = "LOAD CASE :"
        base_ld_case_comb.__init__(self, fpath, self._search_string)
