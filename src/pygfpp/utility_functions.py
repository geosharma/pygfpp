import pandas as pd


def flatten_dict(nested_dict):
    """Ref: tRosenflanz
    https://stackoverflow.com/questions/13575090/construct-pandas-dataframe-from-items-in-nested-dictionary
    """
    res = {}
    if isinstance(nested_dict, dict):
        for k in nested_dict:
            flattened_dict = flatten_dict(nested_dict[k])
            for key, val in flattened_dict.items():
                key = list(key)
                key.insert(0, k)
                res[tuple(key)] = val
    else:
        res[()] = nested_dict
    return res


def dict2df(dict_data: dict) -> pd.DataFrame:
    """Convert nested dict data to pandas DataFrame"""
    column_names: dict = {
        "pfactors": ["p-factor", "y-factor"],
        "pile_disp": [
            "disp-x_in",
            "disp-y_in",
            "disp-z_in",
            "rot-x_rad",
            "rot-y_rad",
            "rot-z_rad",
        ],
        "pile_reaction": [
            "axial_k",
            "lat-y_k",
            "lat-z_k",
            "mom-x_k-in",
            "mom-y_k-in",
            "mom-z_k-in",
        ],
        "lateral_analysis": [
            "disp-x_in",
            "disp-y_in",
            "disp-z_in",
            "disp-yz_in",
            "mom-z_k-in",
            "mom-y_k-in",
            "mom-yz_k-in",
            "shear-y_k",
            "shear-z_k",
            "shear-yz_k",
            "soil-react-y_k/in",
            "soil-react-z_k/in",
            "soil-react-yz_k/in",
            "axial_k",
            "total-stress_k/in2",
            "flex-rigid-z_k-in2",
            "flex-rigid-y_k-in2",
            "dist-load-y_k/in",
            "dist-load-z_k/in",
            "dem/cap_mom",
            "dem/cap_shear",
        ],
    }
    rename_levels = {
        "pfactors": {"level_0": "case", "level_1": "pile_no"},
        "pile_disp": {"level_0": "case", "level_1": "axis", "level_2": "pile_no"},
        "pile_reaction": {"level_0": "case", "level_1": "axis", "level_2": "pile_no"},
        "lateral_analysis": {
            "level_0": "case",
            "level_1": "min_max",
            "level_2": "pile_no",
        },
    }
    key, nested_dict = list(dict_data.items())[0]
    res = flatten_dict(nested_dict)
    df = pd.DataFrame(
        res.values(),
        index=pd.MultiIndex.from_tuples(res.keys()),
        columns=column_names[key],
    )
    df = df.reset_index()
    df = df.rename(columns=rename_levels[key])
    return df
